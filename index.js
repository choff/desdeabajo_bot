
// bot telegram
const Telegraf = require('telegraf');
const token = '723137237:AAEbu2fom1RrD0aDbCzsldaRarc9pB0kj-g';
const bot = new Telegraf(token);

// feeds handler
const Twitt = require('./lib/twitt'),
      User = require('./lib/user'),
      config = require('./config');


const profiles = config.url,
      interval = 1000 * 60 * config.minutes; 

// bandera que previene el envio de mensajes en la primera ejecucion
let init_flag = 1;

// datos en memoria de los ultimos feeds
let DIM = {};
Object.keys(profiles).forEach(source => DIM[source] = {});

// id de usuarios
let usersId;

// carga id de usuarios desde archivo
User.readUsers()
  .then(data => {
    console.log('users from file:', data);
    usersId = [...data];
  });

// maneja los errores del bot
bot.catch((err) => {
  console.log('Demonios!!! ', err);
});

// test 
bot.command('ping', (ctx) => ctx.reply('A luchar y construir un mejor mundo !!'));

bot.start(async (ctx) => {
  let user_id = ctx.message.chat.id;
  // guarda el id del usuario
  if (usersId.indexOf(user_id) == -1) {
    usersId.push(user_id);
    await User.saveUsers(usersId);
  }
  ctx.reply(`Bienvenid@ a la prensa desdeAbajo`);
});

// broadcast message
const sendMsg = function(Msg){
  let ids_to_remove = [];
  usersId.forEach((id) => {
    try {
      bot.telegram.sendMessage(id,Msg);
    }
    catch(err){
      ids_to_remove.push(id);
      console.log('UPPs! ', err);
    }
  });
  // if id of user has no active delete it from active users
  ids_to_remove.forEach(id => {
    let i = usersId.indexOf(id);
    if(i != -1) usersId.splice(i,1);
  });
};

// actualiza los feed y obtiene un objeto con los feeds a enviar
async function updateTwitts(DIM, profiles) {
  let twittsToSend = await Twitt.updateTwittsDIM(DIM, profiles);
  if(!init_flag) {
    Object.keys(twittsToSend).forEach(source => {
      Object.keys(twittsToSend[source]).forEach(guid => {
        sendMsg(twittsToSend[source][guid]);
      });
    });
  } else {
    console.log('primera ejecucion no se envian twitts');
  }
  init_flag = 0;
}

// refresh news
setInterval(updateTwitts, interval, DIM, profiles);


bot.launch();


