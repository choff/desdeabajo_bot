/*
 * Modulo que lee y guarda los usuarios inscritos al bot en un archivo
 */

const fs = require('fs').promises;
const usersFile = require('../config').userFile;

// recibe un arreglo con los id de los usarios para guardarlos en usersFile
async function saveUsers(users) {
  let cad = users.toString();
  // console.log('to write:',cad);
  await fs.writeFile(usersFile, cad, 'utf8');
  return cad;
}

// retorna un arreglo con los ids de los usuarios
async function readUsers(){
  let data = await fs.readFile(usersFile,'utf8');
  let users = data.split(',');

  let idx = users.indexOf('');
  if(idx != -1) users.splice(idx, 1);

  // se convierte el id del usuario a numero
  users = users.map(user => parseInt(user));
  //console.log('readUsers function',users);
  return users; 
}

module.exports = {
  readUsers,
  saveUsers
};

