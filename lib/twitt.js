// DEPENDENCIES
const request = require('request-promise');

// like jQuery but for server
const cheerio = require('cheerio');
const hash = require('hasha');
const config = require('../config');


// get latest twitts from differents profiles
async function getTwitts(profiles) {
  let new_twitts = {};
  let sources = Object.keys(profiles);

  // loop over profiles
  for(let i = 0; i < sources.length; i++) {
    let source = sources[i];
    let uri_profile = profiles[sources];
    // console.log('source:', source);
    let options = {
      uri: uri_profile,
      transform: function (body) {
        return cheerio.load(body);
      }
    };

    // scrap of twitter profile to get tweets
    try {
      let $ = await request(options);
      $('.content').each((i, el)=> {
        // se parsea el mensaje
        let tittle = $(el).
            find('.js-tweet-text-container p').
            text().
            replace('http', ' http').
            replace('pic.twitter', ' pic.twitter');
        let guid = hash(tittle); 

        if(!new_twitts.hasOwnProperty(source))
          new_twitts[source] = {};
        new_twitts[source][guid] = tittle;
      });
      
    }
    catch (err){
      console.log('UPSS!! ',err);
    }
  }
  // console.log('ans:', Object.keys(new_twitts.desdeabajo));
  return new_twitts;

}


// devuelve dos arreglos con los guid a borrar y a agregar
function getChanges(source, newTwitts, DIM) {
  let oldest_guid = Object.keys(DIM[source]);
  let newest_guid = Object.keys(newTwitts[source]);
  let to_delete = [];
  let to_add = [];

  // WARNING this line is only for testing
  // oldest_guid.pop();
  
  // obtiene el guid a borrar
  oldest_guid.forEach(old_guid => {
    // si old_guid no esta en newFeeds
    if(newest_guid.indexOf(old_guid) == -1) 
      to_delete.push(old_guid);
  });

  // obtiene el guid a agregar
  newest_guid.forEach(new_guid => {
    if(oldest_guid.indexOf(new_guid) == -1) 
      to_add.push(new_guid);
  });

  return { to_delete, to_add };
}

// update data in memory
function updateDIM (to_delete, to_add, source, newTwitts, DIM) {
  // delete from DIM
  to_delete.forEach( guid => delete DIM[source][guid]);

  // add to DIM
  to_add.forEach( guid => DIM[source][guid] = {...newTwitts[source][guid]});
}

// actualiza los Feeds en memoria y retorna un objecto con los
// nuevos feeds a enviar
async function updateTwittsDIM(DIM, profiles) {
  let newTwitts = await getTwitts(profiles);
  
  let twittsToSend = {};
  
  Object.keys(newTwitts).forEach(source => {
    let { to_delete, to_add } = getChanges(source, newTwitts, DIM); 
    updateDIM(to_delete, to_add, source, newTwitts, DIM);

    twittsToSend[source] = {};
    to_add.forEach(guid => twittsToSend[source][guid] = newTwitts[source][guid]);
  });

  return twittsToSend;
}

module.exports = {
  updateTwittsDIM
};
