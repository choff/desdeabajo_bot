// config for bot

// perfil de twwiter
let url = {
  desdeabajo: 'https://twitter.com/desdeabajo_info'
};

// interval in minutes to refresh info
let minutes = 5;

// users file
let userFile = __dirname + '/users';


module.exports = {
  url,
  minutes,
  userFile
};
